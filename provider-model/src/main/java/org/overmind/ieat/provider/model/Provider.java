package org.overmind.ieat.provider.model;

import lombok.Value;

@Value
public class Provider {
    String name;
}
