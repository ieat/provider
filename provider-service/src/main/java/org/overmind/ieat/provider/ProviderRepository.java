package org.overmind.ieat.provider;

import org.overmind.ieat.provider.model.Provider;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProviderRepository extends MongoRepository<Provider, String> {
}
