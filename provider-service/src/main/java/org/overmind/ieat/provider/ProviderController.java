package org.overmind.ieat.provider;

import lombok.Value;
import org.overmind.ieat.provider.model.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Value
@RestController
public class ProviderController {

    ProviderService service;

    @GetMapping
    public List<Provider> findAll() {
        return service.findAll();
    }

}
