package org.overmind.ieat.provider;

import lombok.Value;
import org.overmind.ieat.provider.model.Provider;
import org.springframework.stereotype.Service;

import java.util.List;

@Value
@Service
public class ProviderService {

    ProviderRepository repository;

    List<Provider> findAll() {
        return repository.findAll();
    }
    
}
